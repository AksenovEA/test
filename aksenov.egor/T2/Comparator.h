#ifndef T2_COMPARATOR_H
#define T2_COMPARATOR_H
#include "DataStruct.h"
namespace aksenov
{
  bool comparator(const DataStruct& lhs, const DataStruct& rhs);
}
#endif //T2_COMPARATOR_H
